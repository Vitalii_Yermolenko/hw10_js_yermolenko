
const tabName = document.querySelector('.tabs');
const content = document.querySelector('.tabs-content').children;
let index = 0;
console.log(tabName.children);
console.log(content);
tabName.addEventListener('click', (event) =>{
    console.log(event);
     for(tab of tabName.children){
         tab.classList.remove('active');     
     }
    event.target.classList.add('active');
     for (let i = 0; i < content.length; i++) {
        if(event.target.textContent === content[i].dataset.index){
            index = i;
        }
     }
     activeContent (index);
})

function activeContent (index){
    for(tab of content){
        tab.classList.remove('active');
    }
    content[index].classList.add('active');
}